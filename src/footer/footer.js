import React from 'react';
import './footer.css';

export default function Footer(props) {
    return (
    <footer>
        &copy; تمامی حقوق متعلق به لقمه است.
    </footer>
    );
}
