import React from 'react';

export class NameField extends React.Component {
    render() {
        return [
            <div className="col-md-2 col-xl-1 lable">
                <label for="usr">نام:</label>
            </div>,
            <div className="col-md-3 col-xl-4">
                <input type="text" className="form-control" name="firstname" id="name" onChange={(event) => this.props.onChange(event)} />
            </div>
        ];
    }
}

export class FamilyNameField extends React.Component {
    render() {
        return [
            <div className="col-md-3 col-xl-2 lable">
                <label for="usr">نام خانوادگی:</label>
            </div>,
            <div className="col-md-3 col-xl-4">
                <input type="text" className="form-control" name="lastname" id="family" onChange={(event) => this.props.onChange(event)} />
            </div>
        ];
    }
}

export class EmailField extends React.Component {
    render() {
        return (
            this.props.mode === "signup" ?
                [
                    <div className="col-md-2 col-xl-1 lable">
                        <label for="usr">ایمیل:</label>
                    </div>,
                    <div className="col-md-3 col-xl-4">
                        <input type="text" className="form-control" name="email" id="email" onChange={(event) => this.props.onChange(event)} />
                    </div>
                ] : [
                    <div className="col-md-2 col-xl-1 lable">
                        <label for="usr">ایمیل:</label>
                    </div>,
                    <div className="col-md-4 col-xl-4">
                        <input type="text" className="form-control" name="email" id="email" onChange={(event) => this.props.onChange(event)} />
                    </div>
                ]
        );
    }
}

export class PhoneNumField extends React.Component {
    render() {
        return [
            <div className="col-md-3 col-xl-2 lable">
                <label for="usr">شماره تماس:</label>
            </div>,
            <div className="col-md-3 col-xl-4">
                <input type="text" className="form-control" name="phoneNumber" id="phone" onChange={(event) => this.props.onChange(event)} />
            </div>
        ];
    }
}

export class PasswordField extends React.Component {
    render() {
        return (
            this.props.mode === "signup" ?
                [
                    <div className="col-md-2 col-xl-1 lable">
                        <label for="usr">رمز عبور:</label>
                    </div>,
                    <div className="col-md-3 col-xl-4">
                        <input type="password" className="form-control" name="password" id="password" onChange={(event) => this.props.onChange(event)} />
                    </div>
                ] : [
                    <div className="col-md-2 col-xl-1 lable">
                        <label for="usr">رمز عبور:</label>
                    </div>,
                    <div className="col-md-4 col-xl-4">
                        <input type="password" className="form-control" name="password" id="password" onChange={(event) => this.props.onChange(event)} />
                    </div>
                ]
        );
    }
}

export class DuplicatePasswordField extends React.Component {
    render() {
        return [
            <div className="col-md-3 col-xl-2 lable">
                <label for="usr">تکرار رمز عبور:</label>
            </div>,
            <div className="col-md-3 col-xl-4">
                <input type="password" className="form-control" name="duplicatePassword" id="password" onChange={(event) => this.props.onChange(event)} />
            </div>
        ];
    }
}



export default class CreditField extends React.Component {
    render() {
        return (
            <div className="col-md-7 col-xl-7 input-section">
                <div className="form-group">
                    <input type="text" className="form-control" name="extraCredit" id="credit" placeholder="میزان افزایش اعتبار" onChange={(event) => this.props.onChange(event)} />
                </div>
            </div>
        );
    }
}



// placeholder={this.props.searchBoxTitle}