import Axios from 'axios';
import React from 'react';
export default function formatedNum(num) {
  var number = num.toString();
  var result = "";
  for (var i = 0; i < number.length; i++) {
    result = result + convertNumToPersian(number[i]);
  }
  return result;
}


export function convertNumToPersian(n) {
  switch (n) {
    case '0':
      return '۰';
      break;
    case '1':
      return '۱';
      break;
    case '2':
      return '۲';
      break;
    case '3':
      return '۳';
      break;
    case '4':
      return '۴';
      break;
    case '5':
      return '۵';
      break;
    case '6':
      return '۶';
      break;
    case '7':
      return '۷';
      break;
    case '8':
      return '۸';
      break;
    case '9':
      return '۹';
      break;
    case '.':
      return '.';
      break;
    default:
      return '';
  }
}

export class NumOfCartFood extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      num : 0
    }
  }


  componentDidMount() {
    let responseBodyCart = {};
    Axios.get('http://ie.etuts.ir:31536/IELOGHME/Cart/Number', {headers: {Authorization: localStorage.getItem("token")}})
      .then(res => {
        if (res.data.successful) {
          responseBodyCart = res.data;
          this.setState({ num: responseBodyCart.result })
        }
      });
  }

  render(){
    return formatedNum(this.state.num);
  }
}
