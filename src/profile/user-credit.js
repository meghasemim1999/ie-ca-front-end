import React from 'react';
import { useAlert } from 'react-alert';
import axios from 'axios';
import './user-profile.css';
import '../utilities/general.css';
import CreditField from '../utilities/form-component.js';
import AlertError from '../utilities/alert-error';

export default class CreditForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            extraCredit: '',
            isHidden: true
        }

        this.isHidden = true;
        this.handleUserInput = this.handleUserInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.loading = this.loading.bind(this);
    }

    handleUserInput (event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]: value});
    }

    loading(event)
    {
        this.setState({isHidden: false});
        this.handleSubmit(event);
    }

    handleSubmit(event){
        event.preventDefault();
        this.isHidden = false;
        console.log(this.state.isHidden);
        var extraCreditNum = Number(this.state.extraCredit);
        var extraCredit = extraCreditNum;
        if(Number.isInteger(extraCredit))
        {
            axios.post('http://ie.etuts.ir:31536/IELOGHME/User/Credit',null,{data: {extraCredit}, headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if(res.data.successful) {
                    window.location.reload(false);
                }               
                else {
                    AlertError(res.data.exception.localizedMessage);
                }               
            });
        }
        else {
            AlertError("invalid extraCredit");
            //
            this.setState({isHidden: true});
        }
        this.isHidden = true;
        // this.setState({isHidden: true});
    }

    render(){
        return (
            <>   
                <div id="credit" className="tab-pane active"><br />
                    <form onSubmit={this.loading}>
                        <div className="row">
                            <div className="col-md-1 col-xl-1"></div>
                            <CreditField onChange={this.handleUserInput}/>
                            <div className="col-md-3 col-xl-3 button-section" >
                                <input type="submit" className={`btn btn-info ${this.state.isHidden ? '' : 'hidden' }`} value="افزایش" />
                                <button className={`btn btn-info ${this.state.isHidden ? 'hidden' : '' }`} type="button" >
                                    در حال ارتباط...
                                    <span className="spinner-border" role="status" aria-hidden="true"></span>
                                </button>
                            </div>
                            <div className="col-md-1 col-xl-1"></div>
                        </div>
                        <br/>
                    </form>
                </div> 
            </>
        );
    }
}
