import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Nav from 'react-bootstrap/Nav';
import React from 'react';
import CreditForm from './user-credit.js';
import OrdersForm from './user-orders.js';
import Header from '../header/header.js';
import ContentHeader from '../content-header/content-header.js';
import { LinkContainer } from 'react-router-bootstrap';
import './user-profile.css';


export default class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: 1
        };
    }


    componentWillMount() {
        if(!localStorage.getItem("token"))
            window.location.href = "/auth";
    }

    render() {
        return (
            <>
                <Header mode={"InUserProfile"} />
                <div className="container-fluid page-container">
                    <ContentHeader mode={"isProfile"} />
                    <div className="row content-section">
                        <div className="col-xl-2 col-md-2"></div>
                        <div className="col-xl-8 col-md-8 content">
                            <Nav className="navClass" variant="pills" activeKey={this.state.activeTab}>
                                <Nav.Item>
                                    <LinkContainer to="/profile/orders">
                                        <Nav.Link eventKey="2" onSelect={() => this.setState({ activeTab: 2 })}>سفارش ها</Nav.Link>
                                    </LinkContainer>
                                </Nav.Item>
                                <Nav.Item>
                                    <LinkContainer to="/profile/credit">
                                        <Nav.Link eventKey="1" onSelect={() => this.setState({ activeTab: 1 })}>افزایش اعتبار</Nav.Link>
                                    </LinkContainer>
                                </Nav.Item>
                            </Nav>
                            <div className="tab-content">
                                <Switch>
                                    <Route exact path={["/profile", "/profile/credit"]} component={CreditForm} />
                                    <Route path={"/profile/orders"} render={ () => <OrdersForm userId={0}/> } />
                                </Switch>
                            </div>
                        </div>
                        <div className="col-xl-2 col-md-2"></div>
                    </div>
                </div>
            </ >
        );
    }
}