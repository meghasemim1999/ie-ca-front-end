import React, { useState } from 'react';
import { useAlert } from 'react-alert';
import axios from 'axios';
import './user-profile.css';
import formatedNum from '../utilities/utilities.js';
import AlertError from '../utilities/alert-error';
import Modal from 'react-bootstrap/Modal';
import Table from 'react-bootstrap/Table';
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'

export default class OrdersForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: []
        }
    }

    componentDidMount() {
        axios.get('http://ie.etuts.ir:31536/IELOGHME/Order', {headers: {Authorization: localStorage.getItem("token")}})
            .then( res => {
                console.log(res);
                if(res.data.successful)
                {
                    console.log(res.data.result);
                    this.setState({orders: res.data.result});
                }              
                else {
                    AlertError(res.data.exception.localizedMessage);
                }
            });
    }

    render() {
        return (
            <div className="tab-content">
                <div id="orders" className="tab-pane active orders"><br />
                    { this.state.orders.length > 0 &&
                        this.state.orders.map((order, i) =>                            
                        <OrderItem key={i+1} index={formatedNum(i + 1)} order={order} />)
                    }
                </div>
            </div>
        );
    }
}

export class OrderItem extends React.Component {
    componentDidMount() {
        console.log("****************")
        console.log(this.props.order)
    }
    render() {
        return (
            <div className="row order">
                <div className="col-xl-1 col-md-1 col-num">{this.props.index}</div>
                <div className="col-xl-6 col-md-6 ordering-restaurant">{this.props.order.orderRestaurant.name}</div>
                <div className="col-xl-5 col-md-5">
                    <div className="row">
                        <OrderStatus order={this.props.order} />
                    </div>
                </div>
            </div>
        );
    }
}

export function OrderStatus(props) {
    const [show, setShow] = useState(false);
    return (
        props.order.orderState === 'DELIVERED' ? 
        [
            <div className="col-xl col-md">
                <button type="button" className="btn delivered" onClick={() => setShow(true)}>مشاهده فاکتور</button>
            </div>,
            <Modal
                show={show}
                size={"lg"}
                onHide={() => setShow(false)}
                centered
            >
            <Modal.Body>
              <OrderDetails order={props.order}></OrderDetails>
            </Modal.Body>
          </Modal>
         ] :
        props.order.orderState === 'DELIVERY_ON_THE_WAY' ? 
        (
            <div className="col-md col-xl order-status delivering">
                پیک در مسیر
            </div>
        ) :
        props.order.orderState === 'SEARCHING_FOR_DELIVERY' ? 
        (
            <div className="col-md col-xl order-status finding-delivery">
                در جستجوی پیک
            </div>
        ) : ''
    );
}

export class OrderDetails extends React.Component {
    calculateOrderPrice(order) {
        var totalPrice = 0;
        for (let i = 0; i < order.orderedFoods.length; i++) {
            totalPrice += order.orderedFoods[i].price * order.orderedFoods[i].ordersNumber;
        }
        return totalPrice;
    }

    render() {
        return [
            <div className="order-details-title">
                {this.props.order.orderRestaurant.name}
            </div>,
            <Container className="order-details" >
                <Row className="order-details-header">
                    <Col className="order-details-cell" md={2} xl={2}>ردیف</Col>
                    <Col className="order-details-cell" md={6} xl={6}>نام غذا</Col>
                    <Col className="order-details-cell" md={2} xl={2}>تعداد</Col>
                    <Col className="order-details-cell" md={2} xl={2}>قیمت</Col>
                </Row>
                {
                    this.props.order.orderedFoods.map((food, i) =>
                        <OrderDetailItem key={i+1} index={formatedNum(i + 1)} food={food} />)
                }
            </Container>,
            <div className="order-details-price">
                {"جمع کل: " + formatedNum(this.calculateOrderPrice(this.props.order)) + "تومان"}
            </div>
        ];
    }
}

export function OrderDetailItem(props) {
    return (
        <Row className="order-details-item">
            <Col className="order-details-cell" md={2} xl={2}>{props.index}</Col>
            <Col className="order-details-cell" md={6} xl={6}>{props.food.foodName}</Col>
            <Col className="order-details-cell" md={2} xl={2}>{formatedNum(props.food.ordersNumber)}</Col>
            <Col className="order-details-cell" md={2} xl={2}>{formatedNum(props.food.price)}</Col>
        </Row>
    );
}