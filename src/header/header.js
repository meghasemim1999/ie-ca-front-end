import React, { useState } from 'react';
import './header.css';
import Cart from '../cart/cart.js';
import Modal from 'react-bootstrap/Modal';
import Axios from 'axios';
import AlertError from '../utilities/alert-error';
import {formatedNum, NumOfCartFood} from "../utilities/utilities.js";

function handleExit(event) {
    localStorage.clear();
    window.location.href = "/auth";
}

export default function Header(props) {
    const [show, setShow] = useState(false);
    return (
        <nav className="header row">
            {
                props.mode === "Common" ?
                    [
                        <div className="col-md-1 col-xl-1 logo-container">
                            <a href="/"><img className="logo" src={require("../resources/LOGO.png")} /></a>
                        </div>,
                        <div className="col-md-7 col-xl-8"></div>,
                        <div className="col-md-1 col-xl-1 menu-item">
                            <div className="with-badge" onClick={() => setShow(true)}>
                                <CartBtn show={show} onHidden={() => setShow(false)} />
                            </div>
                        </div>,
                        <div className="col-md-2 col-xl-1 menu-item">
                            <a href="/profile">حساب کاربری</a>
                        </div>,
                        <div className="col-md-1 col-xl-1 menu-item exit">
                            <a onClick={(e) => handleExit(e)}>خروج</a>
                        </div>
                    ] :
                    props.mode === "InUserProfile" ?
                        [
                            <div className="col-md-1 col-xl-1 logo-container">
                                <a href="/"><img className="logo" src={require("../resources/LOGO.png")} /></a>
                            </div>,
                            <div className="col-md-9 col-xl-9"></div>,
                            <div className="col-md-1 col-xl-1 menu-item">
                                <div className="with-badge" onClick={() => setShow(true)}>
                                    <CartBtn />
                                </div>
                            </div>,
                            <div className="col-md-1 col-xl-1 menu-item exit">
                                <a onClick={(e) => handleExit(e)}>خروج</a>
                            </div>
                        ] : null
            }
        </nav>
    );
}

export function CartBtn(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return [
        <div onClick={handleShow}>
            <i className="flaticon-smart-cart"></i>
            <span className="badge badge-primary icon-badge"><NumOfCartFood /></span>
        </div>,
        <Modal
            show={show}
            size={"sm"}
            onHide={handleClose}
            centered
        >
            <Modal.Body>
                <HeaderCart/>
            </Modal.Body>
        </Modal>
    ];
}


export class HeaderCart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: null,
        }
        this.handleAddToCartSubmit = this.handleAddToCartSubmit.bind(this);
        this.handleDeleteFromCartSubmit = this.handleDeleteFromCartSubmit.bind(this);
    }

    handleAddToCartSubmit(food) {
        let intendedFood = {
            restaurantName: food.restaurantName,
            foodName: food.name,
            isRegular: "true"
        }

        Axios.post('http://ie.etuts.ir:31536/IELOGHME/Cart', intendedFood, {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if (res.data.successful) {
                    window.location.reload(false);
                }              
                else {
                    AlertError(res.data.exception.localizedMessage);
                }
            });
    }
    handleDeleteFromCartSubmit(food) {
        let intendedFood = {
            restaurantName: food.restaurantName,
            foodName: food.name,
            isRegular: "true"
        }
        console.log(intendedFood);
        Axios.delete('http://ie.etuts.ir:31536/IELOGHME/Cart', { data: intendedFood, headers: {Authorization: localStorage.getItem("token")} })
            .then(res => {
                if (res.data.successful) {
                    window.location.reload(false);
                }                
                else {
                    AlertError(res.data.exception.localizedMessage);
                }
            });
    }
    
    componentDidMount() {
        let responseBodyCart = {};
        Axios.get('http://ie.etuts.ir:31536/IELOGHME/Cart', {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if (res.data.successful) {
                    responseBodyCart = res.data;
                    this.setState({ cart: responseBodyCart.result })
                }              
                else {
                    // AlertError(res.data.exception.localizedMessage);
                }
            });
    }
    render() {
        return <Cart cart={this.state.cart} onAddToCartClick={this.handleAddToCartSubmit} onDelFromCartClick={this.handleDeleteFromCartSubmit} />
    }
}