import React from 'react';
import { useAlert } from 'react-alert';
import Header from '../header/header.js';
import ContentHeader from '../content-header/content-header.js';
import './restaurant.css';
import Cart from '../cart/cart.js';
import RestaurantMenu from './menu.js';
import Axios from "axios";
import AlertError from '../utilities/alert-error';

export default class Restaurant extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restaurant: null,
            cart: null
        };

        this.handleAddToCartSubmit = this.handleAddToCartSubmit.bind(this);
        this.handleDeleteFromCartSubmit= this.handleDeleteFromCartSubmit.bind(this);
    }


    componentWillMount() {
        if(!localStorage.getItem("token"))
            window.location.href = "/auth";
    }
    
    handleAddToCartSubmit(food){
        let intendedFood={
            restaurantName: food.restaurantName,
            foodName: food.name,
            isRegular: "true"
        }

        Axios.post('http://ie.etuts.ir:31536/IELOGHME/Cart',intendedFood, {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if(res.data.successful) {
                    window.location.reload(false);
                }                  
                else {
                    AlertError(res.data.exception.localizedMessage);
                }            
            });
    }
    handleDeleteFromCartSubmit(food){
        let intendedFood={
            restaurantName: food.restaurantName,
            foodName: food.name,
            isRegular: "true"
        }
        console.log(intendedFood);
        Axios.delete('http://ie.etuts.ir:31536/IELOGHME/Cart',{ data: intendedFood }, {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if(res.data.successful) {
                    window.location.reload(false);
                }                
                else {
                    AlertError(res.data.exception.localizedMessage);
                }              
            });
    }

    

    componentDidMount() {
        console.log(this.props.location.state.id);
        let responseBodyRestaurant = {};
        let responseBodyCart = {};
        Axios.get('http://ie.etuts.ir:31536/IELOGHME/Restaurant?id=' + this.props.location.state.id, {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if(res.data.successful){
                    responseBodyRestaurant = res.data;
                    this.setState({restaurant: responseBodyRestaurant.result})
                }              
                else {
                    AlertError(res.data.exception.localizedMessage);
                }
                
                //TODO exception handling
            });
        Axios.get('http://ie.etuts.ir:31536/IELOGHME/Cart', {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if(res.data.successful){
                    responseBodyCart = res.data;
                    this.setState({cart: responseBodyCart.result})
                }              
                else {
                    // AlertError(res.data.exception.localizedMessage);
                }
                //TODO exception handling
            });   
    }

    render() {
        return (
            <>
                <Header mode={"Common"} />
                <div className="container-fluid page-container">
                    <ContentHeader mode={"isEmpty"} />
                    { this.state.restaurant && (
                        <>
                    <RestaurantInfo name={this.state.restaurant.name} logo={this.state.restaurant.logo} />
                    <div className="title main-title menu-title">منوی غذا</div>
                    <div className="row fluid">
                        <div className="col-md-2 col-xl-3 cart">
                            <Cart cart={this.state.cart} onAddToCartClick={this.handleAddToCartSubmit} onDelFromCartClick={this.handleDeleteFromCartSubmit}/>
                        </div>
                        <RestaurantMenu cart={this.state.cart}
                                        restaurantName={this.state.restaurant.name} 
                                        menu={this.state.restaurant.regularMenu} 
                                        onAddToCartClick={this.handleAddToCartSubmit}
                                        onDelFromCartClick={this.handleDeleteFromCartSubmit}/>
                    </div> </>)
                    }
                </div>
            </ >
        );
    }
}

export function RestaurantInfo(props) {
    return (
        <div className="row fluid">
            <div className="col-md col-xl fluid">
                <div className="row fluid">
                    <div className="col-md col-xl fluid restaurant-info">
                        <img className="restaurant-logo" src={props.logo} />
                    </div>
                </div>
                <div className="row fluid">
                    <div className="col-md col-xl fluid restaurant-info restaurant-name font-weigh-bolder">
                        {props.name}
                    </div>
                </div>
            </div>
        </div>
    );
}
