import React, { useState } from 'react';
import './restaurant.css';
import formatedNum from "../utilities/utilities.js";
import Modal from 'react-bootstrap/Modal';
import './menu.css'
export default class RestaurantMenu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            status : "exist",
        }
    }
    render() {
        return (
            <div className="col-md-9 col-xl-8 menu">
                {
                    this.props.menu.map((menuFood, i) => <MenuItemWithModal key={i} 
                                                                            menuFood={menuFood} 
                                                                            restaurantName={this.props.restaurantName} 
                                                                            number={this.findNumInCart(menuFood.name)} 
                                                                            status={this.state.status} 
                                                                            onAddToCartClick={this.props.onAddToCartClick}
                                                                            onDelFromCartClick={this.props.onDelFromCartClick}/>)
                }
            </div>
        );
    }

    findNumInCart(foodName){
        if(this.props.cart === null)
            return 0;
        let cart = this.props.cart;
        for (let i = 0; i < cart.length; i++) {
            const food = cart[i];
            if(food.name === foodName)
                return food.ordersNumber;
        }
        return 0;
    }
}

export function MenuItemWithModal(props) {
    const [show, setShow] = useState(false);
    return [
        <MenuItem food={props.menuFood} onAddToCartClick={props.onAddToCartClick} onClick={() => setShow(true)} status ={props.status}/>,
        <Modal
            show={show}
            size={"m"}
            onHide={() => setShow(false)}
            centered
        >
            <Modal.Body>
                <FoodDetails food={props.menuFood} restaurantName={props.restaurantName}></FoodDetails>
            </Modal.Body>
            <Modal.Footer>
            {
                !props.menuFood.doesExist ?
                null :
                [
                    <a>
                        <i className="flaticon-minus" onClick={() => props.onDelFromCartClick(props.menuFood)}></i>
                    </a>,
                    <div>{' ' + formatedNum(props.number) + ' '}</div>,
                    <a>
                        <i className="flaticon-plus" onClick={() => props.onAddToCartClick(props.menuFood)} ></i>
                    </a>
                ]
            }
            {
                props.menuFood.doesExist ?
                    <input type="submit" className="button-det" value=" اضافه کردن به سبد خرید" onClick={() => props.onAddToCartClick(props.menuFood)}/>
                    :
                    <input type="button" className="button button-not-exist" value="ناموجود" disabled/>
            }
            
            </Modal.Footer>
        </Modal>
    ];
}

export function FoodDetails(props) {
    return [
        <div className="restaurant-name-det">{props.restaurantName}</div>,
        <div className="row">
            <div className="col-md-2 col-xl-2">
                <img className="food-image-det" src={props.food.image} alt="food image" />
            </div>
            <div className="col-md-10 col-xl-10">
                <div className="row">
                    < div className="col-md-1 col-xl-1"></div>
                    <div className="col-md-11 col-xl-11 details">
                        <div className="food-name-det">{props.food.name}  </div>
                        <div className="food-star">
                            <i className="fas fa-star"></i>
                        </div>
                        {formatedNum(props.food.popularity)}
                        <div className="food-det">{props.food.description}</div>
                        <div className="food-price-det">{formatedNum(props.food.price)} تومان</div>
                    </div>
                </div>
            </div>
        </div >
    ];
}

export class MenuItem extends React.Component {
    
    render() {
        return (
            <div>
                <img className="food-image" src={this.props.food.image} alt="food image"  onClick={this.props.onClick}/>
                <div className="row fluid">
                    <div className="col-md-8 col-xl-8 food-name">
                        {this.props.food.name}
                    </div>
                    <div className="col-md-2 col-xl-2 food-number">
                        {formatedNum(this.props.food.popularity)}
                        <i className="fas fa-star food-star"></i>
                    </div>
                </div>
                <div className="food-price">
                    {formatedNum(this.props.food.price)} تومان
            </div>
                <FoodStatusBtn status={this.props.status} onAddToCartClick={this.props.onAddToCartClick} food={this.props.food}/>
            </div>
        );
    }
}

export class FoodStatusBtn extends React.Component {
    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event){
        event.preventDefault();
        this.props.onAddToCartClick(this.props.food);
    }
    render(){
        return (
            this.props.status === 'exist' ?
            (
                <div>
                    <input type="button" className="button" value="افزودن به سبد خرید" onClick={this.handleClick} />
                </div>
            ) :
            this.props.status === 'notExist' ?
            (
                <div>
                    <input type="button" className="button button-not-exist" value="ناموجود" />
                </div>
            ) : ''
        );
    }
}