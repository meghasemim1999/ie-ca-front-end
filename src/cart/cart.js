import React from 'react';
import Axios from 'axios';
import './cart.css';
import formatedNum from "../utilities/utilities.js";
import AlertError from '../utilities/alert-error';


export default class Cart extends React.Component {
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    findTotalPrice() {
        if(this.props.cart === null)
            return 0;
        let cart = this.props.cart;
        let totalPrice = 0;
        for (let i = 0; i < cart.length; i++) {
            totalPrice += cart[i].ordersNumber * cart[i].price;
        }
        return totalPrice;
    }

    handleSubmit(){
        Axios.post('http://ie.etuts.ir:31536/IELOGHME/Order',null, {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if(res.data.successful) {
                    window.location.reload(false);
                }              
                else {
                    AlertError(res.data.exception.localizedMessage);
                }
            });
    }

    render() {
        return (
            <div className="row ">
                <div className="col-md col-xl">
                    <div className="title cart-title">
                        سبد خرید
                    </div> 
                    {this.props.cart &&(
                    <>               
                    <div className="cart-content">
                        {
                            this.props.cart.map((cartFood, i) =>
                                <CartItem key={i} cartFood={cartFood} onAddToCartClick={this.props.onAddToCartClick} onDelFromCartClick={this.props.onDelFromCartClick}/>)
                        }
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-md col-xl cart-foods-cost">
                            جمع کل:
                            <inline className="total-price">{formatedNum(this.findTotalPrice())} تومان</inline>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-2 col-xl-2"></div>
                        <div className="col-md-8 col-xl-8 butten-col">
                            <input type="button" className="btn btn-info cart-btn-info" value="تایید نهایی" onClick={this.handleSubmit}/>
                        </div>
                        <div className="col-md-2 col-xl-2"></div>
                    </div>
                    </>)}
                </div>
            </div>
        );
    }
}


export class CartItem extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-4 col-xl-7 cart-food-name">{this.props.cartFood.foodName}</div>
                <div className="col-md-8 col-xl-5">
                    <a >
                        <i className="flaticon-minus" onClick={() => this.props.onDelFromCartClick({name:this.props.cartFood.foodName, restaurantName: this.props.cartFood.restaurantName})}></i>
                    </a>
                    {' ' +  formatedNum(this.props.cartFood.ordersNumber) + ' '}
                    <a >
                        <i className="flaticon-plus" onClick={() => this.props.onAddToCartClick({name:this.props.cartFood.foodName, restaurantName: this.props.cartFood.restaurantName})}></i>
                    </a>
                    <div className="row">
                        <div className="col-md col-xl food-price-cart">
                            {formatedNum(this.props.cartFood.price)}تومان
                        </div>
                    </div>
                </div>
                <div className="line"></div>
            </div>
        );
    }

}