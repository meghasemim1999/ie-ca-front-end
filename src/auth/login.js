import React from 'react';
import './signup.css';
import { PasswordField, EmailField } from '../utilities/form-component.js';
import FormErrors from '../utilities/form-errors.js';
import Axios from 'axios';
import AlertError from '../utilities/alert-error';
import { gapi } from 'gapi-script';

export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            formErrors: {email: '', password: ''},
            emailValid: false,
            passwordValid: false,
            formValid: false
        }

        this.handleUserInput = this.handleUserInput.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.onGSignIn = this.onGSignIn.bind(this);
    }

    handleUserInput (event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]: value},
            () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
      
        switch(fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;
            case 'password':
                passwordValid = this.state.password.length >= 8;
                fieldValidationErrors.password = passwordValid ? '': ' is too short';
                break;
          default:
                break;
        }
        this.setState({formErrors: fieldValidationErrors,
                        emailValid: emailValid,
                        passwordValid: passwordValid,
                      }, this.validateForm);
    }
      
    validateForm() {
    this.setState({formValid: this.state.emailValid && this.state.passwordValid});
    }

    handleLogin(event){
        event.preventDefault();
        let responseBody = [];
        let requestBody = {email: this.state.email, password: this.state.password};
        Axios.post('http://ie.etuts.ir:31536/IELOGHME/Auth', requestBody)
        .then(res => {
            if(res.status == 403) {
                AlertError("Email or Password is not correct.");
                return;
            }
            if(res.data.successful) {
                responseBody = res.data;
                localStorage.setItem("token","bearer " + responseBody.result);
                window.location.href = "/";
            }               
            else {
                AlertError(res.data.exception.localizedMessage);
            }               
        });
    }

    onGSignIn(googleUser) {
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("injaaaaaaaaaaaaaaaAA")
        console.log(id_token)
        Axios.post('http://ie.etuts.ir:31536/IELOGHME/Auth/Google', {token: id_token}).then(
            res => {
                if(res.status == 403) {
                    AlertError("Email or Password is not correct.");
                    return;
                }
                if(res.data.successful) {
                    var responseBody = res.data;
                    localStorage.setItem("token","bearer " + responseBody.result);
                    var auth2 = gapi.auth2.getAuthInstance();
                    auth2.signOut().then(function () {
                        console.log('User signed out.');
                    });
                    window.location.href = "/";
                }               
                else {
                    AlertError(res.data.exception.localizedMessage);
                }
            }
        );
    }

    componentDidMount() {
        gapi.signin2.render('g-signin2', {
			'scope': 'profile email',
			'width': 200,
			'height': 50,
			'onsuccess': this.onGSignIn
		});
    }

    render(){
        return [
            <div className="panel panel-default" >
                <FormErrors formErrors={this.state.formErrors} />
            </div>,
            <form onSubmit={this.handleLogin}>
                <div className="form-group">
                    <div className="row">
                        <div className="col-md-2 col-xl-3 lable"></div>
                        <EmailField onChange={this.handleUserInput} mode={"login"}/>
                        <div className="col-md-4 col-xl-4 lable"></div>
                    </div>
                    <div className="row">
                        <div className="col-md-2 col-xl-3 lable"></div>
                        <PasswordField onChange={this.handleUserInput} mode={"login"}/>
                        <div className="col-md-4 col-xl-4 lable"></div>
                    </div>
                </div>
                <br/>
                <div className="button-section">
                    <input type="submit" disabled={!this.state.formValid} className="btn btn-info" value="ورود" />
                    <br/>
                    <div id="g-signin2" className="gloginbtn" data-onsuccess={this.onGSignIn} />
                </div>
                <br/>
            </form>
        ];
    }
}
