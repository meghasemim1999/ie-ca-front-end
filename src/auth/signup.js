import React from 'react';
import './signup.css';
import FormErrors from '../utilities/form-errors.js';
import {EmailField,NameField, FamilyNameField, PhoneNumField, PasswordField, DuplicatePasswordField} from  '../utilities/form-component.js';
import Axios from 'axios';
import AlertError from '../utilities/alert-error';
import {withRouter} from "react-router-dom"


export default class SignupForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            phoneNumber: '',
            password: '',
            duplicatePassword: '',
            formErrors: {email: '', phoneNumber: '', password: ''},
            emailValid: false,
            phoneNumberValid: false,
            passwordValid: false,
            formValid: false,
        }

        this.handleUserInput = this.handleUserInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleUserInput (event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]: value},
            () => { name === 'duplicatePassword' ?  this.validateField('password', value) : this.validateField(name, value) });
    }

    
    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
        let phoneNumberValid = this.state.phoneNumberValid;
      
        switch(fieldName) {
            case 'phoneNumber':
                phoneNumberValid = value.length === 11 && value[0] === '0' && value[1] === '9';
                fieldValidationErrors.phoneNumber = phoneNumberValid ? '' : ' must be 11 digits and start with 09';
                break;
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;
            case 'password':
                passwordValid = this.state.password.length >= 8;
                fieldValidationErrors.password = passwordValid ? '': ' is too short';
                if(!passwordValid)
                    break;
                else {
                    passwordValid = this.state.password === this.state.duplicatePassword;
                    fieldValidationErrors.password = passwordValid ? '': ' is not equal to duplicate password';
                }
                break;
          default:
                break;
        }
        this.setState({formErrors: fieldValidationErrors,
                        emailValid: emailValid,
                        passwordValid: passwordValid,
                        phoneNumberValid: phoneNumberValid
                      }, this.validateForm);
      }
      
      validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid && this.state.phoneNumberValid});
      }


      handleSubmit(event){
        event.preventDefault();
        let user={
            firstname:this.state.firstname,
            lastname:this.state.lastname,
            phoneNumber:this.state.phoneNumber,
            email:this.state.email,
            password:this.state.password
        }
        Axios.post('http://ie.etuts.ir:31536/IELOGHME/User', user)
        .then(res => {
            if(res.data.successful) {
                window.location.href = "/auth/login";
            }else{
                if(res.data.exception) {
                    AlertError(res.data.exception.localizedMessage);
                }else{
                    AlertError(res.data.result);
                }               
            }              
        });    
    }


    render() {
        return [
            <div className="panel panel-default" >
                <FormErrors formErrors={this.state.formErrors} />
            </div>,
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <div className="row">
                        <NameField onChange={this.handleUserInput}/>
                        <div className="d-md-none d-xl-block col-xl-1"></div>
                        <FamilyNameField onChange={this.handleUserInput}/>
                    </div>
                    <div className="row">
                        <EmailField onChange={this.handleUserInput} mode={"signup"}/>
                        <div className="d-md-none d-xl-block col-xl-1"></div>
                        <PhoneNumField onChange={this.handleUserInput}/>
                    </div>
                    <div className="row">
                        <PasswordField onChange={this.handleUserInput} mode={"signup"}/>
                        <div className="d-md-none d-xl-block col-xl-1"></div>
                        <DuplicatePasswordField onChange={this.handleUserInput}/>
                    </div>
                </div>
                <br/>
                <div className="button-section">
                    <input type="submit" disabled={!this.state.formValid} className="btn btn-info" value="ثبت نام" /> 
                </div>
                <br/>
            </form>
        ];
    }
}

