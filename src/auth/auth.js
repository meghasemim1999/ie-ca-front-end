import { BrowserRouter as Route, Switch} from "react-router-dom";
import { LinkContainer } from 'react-router-bootstrap';
import Nav from 'react-bootstrap/Nav';
import React from 'react';
import LoginForm from './login.js';
import SignupForm from './signup.js';
import Header from '../header/header.js';
import ContentHeader from '../content-header/content-header.js';
import './auth.css';


export default class Authentication extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: 1
        };
    }


    componentWillMount() {
        if(localStorage.getItem("token"))
            window.location.href = "/";
    }

    render() {
        return (
            <>           
                <Header mode={"empty"}/>
                <div className="container-fluid page-container">
                    <ContentHeader mode={"withLogo"}/>    
                    <br/>
                    <div className="row content-section">
                        <div className="col-xl-2 col-md-1"></div>
                        <div className="col-xl-8 col-md-10 content">
                            <Nav className="navClass" variant="pills" activeKey={this.state.activeTab}>
                                <Nav.Item>
                                    <LinkContainer to="/auth/login">
                                        <Nav.Link eventKey="1" onSelect={() => this.setState({activeTab: 1})}>ورود</Nav.Link>
                                    </LinkContainer>
                                </Nav.Item>
                                <Nav.Item>
                                    <LinkContainer to="/auth/signup">
                                        <Nav.Link eventKey="2" onSelect={() => this.setState({activeTab: 2})}>ثبت نام</Nav.Link>
                                    </LinkContainer>
                                </Nav.Item>
                            </Nav>
                            <div className="tab-content">
                                <div id="auth-content" className="tab-pane active"><br/>
                                    <div className="row">
                                        <div className="col-md-1 col-xl-1"></div>
                                        <div className="col-md-10 col-xl-10">
                                            <br/>
                                            <Switch>
                                                <Route exact path={["/auth", "/auth/login"]} >
                                                    <LoginForm/>
                                                </Route>
                                                <Route path={"/auth/signup"} >
                                                    <SignupForm/>
                                                </Route>
                                            </Switch>
                                        </div>
                                        <div className="col-md-1 col-xl-1"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-2 col-md-1"></div>
                    </div>
                </div>
            </>
        );
    }
}