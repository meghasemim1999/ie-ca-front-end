import React from 'react';
import axios from 'axios';
import LoadingOverlay from 'react-loading-overlay';
import formatedNum from '../utilities/utilities.js';
import AlertError from '../utilities/alert-error';
import './content-header.css';

export default class ContentHeader extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            user : {
                firstname: '',
                lastname: '',
                email: '',
                phoneNumber: '',
                credit: ''
            },
            isLoading: true
        }
    }

    componentDidMount() {
        if(this.props.mode === "isProfile")
        {
            let responseBody = {};
            axios.get('http://ie.etuts.ir:31536/IELOGHME/User', {headers: {Authorization: localStorage.getItem("token")}})
                .then( res => {
                    if(res.data.successful)
                    {
                        responseBody = res.data;
                        this.setState({user: responseBody.result});
                    }              
                    else {
                        AlertError(res.data.exception.localizedMessage);
                    }
                    

                    this.setState({isLoading: false});
                });
        }
    }
    
    formatedName(){
        return this.state.user.firstname + " " + this.state.user.lastname;
    }
    
    render(){
        return (
            <LoadingOverlay
                active={this.state.isLoading && this.props.mode === "isProfile"}
                spinner
                styles={{
                    overlay: (base) => ({
                    ...base,
                    background: 'rgba(255, 255, 255, 0.4)'
                    })
                }}
            >
            { this.props.mode === 'isHome' ? (
                <div className="row content-header cover">
                    <div className="cover-content">
                        <img className="cover-logo" src={require("../resources/LOGO.png")} />
                        <p>اولین و بزرگترین وبسایت سفارش آنلاین غذا در دانشگاه تهران</p>
                    </div>
                </div>
            ):
            this.props.mode === 'isProfile'?
            (
                <div className="row redSection">
                    <div className="col-md-8 col-xl-8">
                        <div className="row">
                            <div className="col-md-3 col-xl-3 iconInCol">
                                <i className="flaticon-account"></i>
                            </div>
                            <div className="col-md-9 col-xl-9 dataInCol username">
                                {this.formatedName()}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-xl-4">
                        <div className="row">
                            <div className="col-md-2 col-xl-2">
                                <i className="flaticon-phone"></i>
                            </div>
                            <div className="col-md-10 col-xl-10 dataInCol">
                                {formatedNum(this.state.user.phoneNumber)}
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-2 col-xl-2">
                                <i className="flaticon-mail"></i>
                            </div>
                            <div className="col-md-10 col-xl-10 dataInCol">
                                {this.state.user.email}
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-2 col-xl-2">
                                <i className="flaticon-card"></i>
                            </div>
                            <div className="col-md-10 col-xl-10 dataInCol">
                            {formatedNum(this.state.user.credit)} تومان
                            </div>
                        </div>
                    </div>
                </div>
            ):
            this.props.mode === "withLogo"?  
            (
                <div className="content-header">
                    <div className="cover-content">
                        <img className="cover-logo" src={require("../resources/LOGO.png")} />
                        <p>اولین و بزرگترین وبسایت سفارش آنلاین غذا در دانشگاه تهران</p>
                    </div>
                </div>

            )
            :
            (
                <div className="row fluid redSection"></div>
            ) }
            </LoadingOverlay>   
        );
    }
}

