import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Footer from '../footer/footer.js';
import Authentication from '../auth/auth.js';
import './app.css';
import Profile from '../profile/user-profile.js';
import Restaurant from '../restaurant/restaurant.js';
import Home from '../home/home.js';

export default class App extends React.Component {
    render() {
        return (
            <Router>
                <React.Fragment>
                    <Switch>
                        <Route exact path="/">
                            <Home />
                        </Route>
                        <Route path="/auth">
                            <Authentication />
                        </Route>
                        <Route path="/profile">
                            <Profile />
                        </Route>
                        <Route path="/restaurant" render={props =>
                            <Restaurant {...props}/>
                        }>
                        </Route>
                    </Switch>
                    <Footer />
                </React.Fragment>
            </Router>
        );
    }
}

