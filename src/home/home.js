import React from 'react';
import Header from '../header/header.js';
import ContentHeader from '../content-header/content-header.js';
import './home.css';
import Restaurants from './restaurants.js';
import FoodParty from './foodParty.js';
import SearchBox from './search-box.js';
import Axios from 'axios';
import AlertError from '../utilities/alert-error';

export default class Home extends React.Component {
   constructor(props) {
        super(props);
        this.state = {
            restaurantsList: []
        };
        this.handleSearch = this.handleSearch.bind(this);
    }

    componentWillMount() {
        if(!localStorage.getItem("token"))
            window.location.href = "/auth";
    }

    render() {
        return (
            <>
                <Header mode={"Common"} />
                <div className="container-fluid page-container">
                    <ContentHeader mode={"isHome"} />
                    <SearchBox onSearch={this.handleSearch}/>
                    <FoodParty />
                    <Restaurants restaurantsList = {this.state.restaurantsList} getRestaurantList={this.getAllRestaurant}/>
                </div>
            </ >
        );
    }

    componentDidMount() {
        let responseBody = [];
        Axios.get('http://ie.etuts.ir:31536/IELOGHME/Restaurant/All', {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                console.log("88888888888888888888888888888888888888888888888888")
                console.log(res)
                if (res.data.successful) {
                    responseBody = res.data;
                    this.setState({ restaurantsList: responseBody.result })
                }              
                else {
                    AlertError(res.data.exception.localizedMessage);
                }
                //TODO exception handling
            })
    }



    handleSearch(event, searchByRestaurant,searchByFood){
        event.preventDefault();
        let responseBody = [];
        console.log(searchByRestaurant +  searchByFood);
        var searchFrom, searchString;
        if(searchByRestaurant !== ''){
            searchFrom = 'R';
            searchString = searchByRestaurant;
        }else{
            searchFrom = 'F';
            searchString = searchByFood;
        }
        if(searchString !== '')
        {
            Axios.get('http://ie.etuts.ir:31536/IELOGHME/Search?searchString=' + searchString + '&searchFrom=' + searchFrom, {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if(res.data.successful) {
                    responseBody = res.data;
                    this.setState({ restaurantsList: responseBody.result })
                }               
                else {
                    AlertError(res.data.exception.localizedMessage);
                }               
            });
        }
        else {
            AlertError("invalid search");
        }
    }
    
}

