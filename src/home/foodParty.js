import React, { useState } from 'react';
import './food-party.css';
import Axios from "axios";
import AlertError from '../utilities/alert-error';
import formatedNum from "../utilities/utilities.js";
import Modal from 'react-bootstrap/Modal';
export default class FoodParty extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            foodPartyMenu: null,
            cart: null
        };

        this.handleAddToCartSubmit = this.handleAddToCartSubmit.bind(this);
        this.handleDeleteFromCartSubmit = this.handleDeleteFromCartSubmit.bind(this);

    }

    handleAddToCartSubmit(food) {
        let intendedFood = {
            restaurantName: food.restaurantName,
            foodName: food.name,
            isRegular: "false"
        }

        Axios.post('http://ie.etuts.ir:31536/IELOGHME/Cart', intendedFood, {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if (res.data.successful) {
                    window.location.reload(false);
                }
                else {
                    AlertError(res.data.exception.localizedMessage);
                }
            });
    }


    handleDeleteFromCartSubmit(food) {
        let intendedFood = {
            restaurantName: food.restaurantName,
            foodName: food.name,
            isRegular: "true"
        }
        console.log(intendedFood);
        Axios.delete('http://ie.etuts.ir:31536/IELOGHME/Cart', { data: intendedFood, headers: {Authorization: localStorage.getItem("token")} })
            .then(res => {
                if (res.data.successful) {
                    window.location.reload(false);
                }
                else {
                    AlertError(res.data.exception.localizedMessage);
                }
            });
    }


    componentDidMount() {
        // console.log(this.props.location.state.id);
        let responseBodyFoodParty = {};
        let responseBodyCart = {};
        Axios.get('http://ie.etuts.ir:31536/IELOGHME/Restaurant/FoodParty', {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if (res.data.successful) {
                    responseBodyFoodParty = res.data;
                    this.setState({ foodPartyMenu: responseBodyFoodParty.result })
                }
                else {
                    AlertError(res.data.exception.localizedMessage);
                }

                //TODO exception handling
            });
        Axios.get('http://ie.etuts.ir:31536/IELOGHME/Cart', {headers: {Authorization: localStorage.getItem("token")}})
            .then(res => {
                if (res.data.successful) {
                    responseBodyCart = res.data;
                    this.setState({ cart: responseBodyCart.result })
                }
                else {
                    // AlertError(res.data.exception.localizedMessage);
                }
                //TODO exception handling
            });
        console.log(this.state.foodPartyMenu)
    }


    findNumInCart(foodName) {
        if (this.state.cart === null)
            return 0;
        let cart = this.state.cart;
        for (let i = 0; i < cart.length; i++) {
            const food = cart[i];
            if (food.name === foodName)
                return food.ordersNumber;
        }
        return 0;
    }

    render() {
        return [
            <br />,
            <div className="row fluid">
                <div className="col-md-5 col-md-5"></div>
                <div className="col-md-2 col-md-2">
                    <div className="home-title">جشن غذا</div>
                </div>
                <div className="col-md-5 col-md-5"></div>

                <div className="horizontal-scroll .food-party-list-item">
                    {
                        (this.state.foodPartyMenu !== null) &&
                        this.state.foodPartyMenu.map((food, i) => <FoodPartyItemWithModal key={i}
                            food={food}
                            number={this.findNumInCart(food.name)}
                            onAddToCartClick={this.handleAddToCartSubmit}
                            onDelFromCartClick={this.handleDeleteFromCartSubmit} />)
                    }
                </div>
            </div>,
        ];
    }
}


export function FoodPartyItemWithModal(props) {
    const [show, setShow] = useState(false);
    return [
        <FoodPartyItem food={props.food} onAddToCartClick={props.onAddToCartClick} onClick={() => setShow(true)} />,
        <Modal
            show={show}
            size={"m"}
            onHide={() => setShow(false)}
            centered
        >
            <Modal.Body>
                <FoodPartyDetails food={props.food} restaurantName={props.restaurantName}></FoodPartyDetails>
            </Modal.Body>
            <Modal.Footer>
            {
                (!props.food.doesExist) ?
                null :
                [
                    <a>
                        <i className="flaticon-minus" onClick={() => props.onDelFromCartClick(props.food)}></i>
                    </a>,
                    <div>{' ' + formatedNum(props.number) + ' '}</div>,
                    <a>
                        <i className="flaticon-plus" onClick={() => props.onAddToCartClick(props.food)} ></i>
                    </a>
                ]
            }
            {
                props.food.doesExist ?
                    <input type="submit" className="button-det" value=" اضافه کردن به سبد خرید" onClick={() => props.onAddToCartClick(props.food)}/>
                    :
                    <input type="button" className="button button-not-exist" value="ناموجود" disabled/>
            }
            
            </Modal.Footer>
        </Modal>
    ];
}

export function FoodPartyDetails(props) {
    return [
        <div className="restaurant-name-det">{props.restaurantName}</div>,
        <div className="row">
            <div className="col-md-2 col-xl-2">
                <img className="food-image-det" src={props.food.image} alt="food image" />
            </div>
            <div className="col-md-10 col-xl-10">
                <div className="row">
                    < div className="col-md-1 col-xl-1"></div>
                    <div className="col-md-11 col-xl-11 details">
                        <div className="food-name-det">{props.food.name}  </div>
                        <div className="food-star">
                            <i className="fas fa-star"></i>
                        </div>
                        {formatedNum(props.food.popularity)}
                        <div className="food-det">{props.food.description}</div>
                        <div className="food-price-det">{formatedNum(props.food.price)} تومان</div>
                    </div>
                </div>
            </div>
        </div >
    ];
}

export class FoodPartyItem extends React.Component {

    render() {
        return (
            <div className="foodParty-item" onClick={this.props.onClick}>
                <div className="row">
                    <div className='col-md-5 col-xl-5'>
                        <img className="food-image" src={this.props.food.image} alt="food image" onClick={this.props.onClick} />
                    </div>
                    <div className='col-md-7 col-xl-7'>
                        <div className="food-name">
                            {this.props.food.name}
                        </div>
                        <div className="food-popularity">
                            {formatedNum(this.props.food.popularity)}
                            <i className="fas fa-star food-star"></i>
                        </div>
                    </div>
                </div>
                <div className="row prices">
                    <div className='col-md-2 col-xl-2'></div>
                    <div className='col-md-4 col-xl-4 food-old-price'>
                        {formatedNum(this.props.food.oldPrice)}
                    </div>
                    <div className='col-md-4 col-xl-4 price'>
                        {formatedNum(this.props.food.price)}
                    </div>
                    <div className='col-md-2 col-xl-2'></div>
                </div>
                <FoodPartyBtns onAddToCartClick={this.props.onAddToCartClick} food={this.props.food} />
                <div className='dashed-line'></div>
                <div className="row restaurantName">
                    <div className='col-md col-xl '>
                        {this.props.food.restaurantName}
                    </div>
                </div>
            </div>
        );
    }
}

export class FoodPartyBtns extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        event.preventDefault();
        this.props.onAddToCartClick(this.props.food);
    }

    render() {
        if (this.props.food.number == 0 || !this.props.food.doesExist) {
            return (
                <div className='row buttons'>
                    <div className='col-md-6 col-xl-6'>
                        <input type="button" className="button existance" value='ناموجود' disabled />
                    </div>
                    <div className='col-md-6 col-xl-6'>
                        <input type="button" className="button no-buy" value='خرید' disabled />
                    </div>
                </div>
            );
        }
        return (
            <div className='row buttons'>
                <div className='col-md-6 col-xl-6'>
                    <input type="button" className="button existance" value={"موجودی:" + formatedNum(this.props.food.count)} />
                </div>
                <div className='col-md-6 col-xl-6'>
                    <input type="button" className="button buy" value='خرید' onClick={this.handleClick} />
                </div>
            </div>);
    }
}