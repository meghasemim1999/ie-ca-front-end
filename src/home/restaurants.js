import { BrowserRouter as Router, Link} from "react-router-dom";
import { useAlert } from 'react-alert';
import React from 'react';
import AlertError from '../utilities/alert-error';
import './restaurants.css';

export default class Restaurants extends React.Component {
    constructor(props) {
        super(props);
        
    }

    
 
    render() {
        return (
            <div className="row fluid">
                <div className="col-md col-xl">
                    <div className="row">
                        <div className="col-md-5 col-xl-5"></div>
                        <div className="col-md-2 col-xl-2">
                            <div className="home-title">رستوران ها</div>
                        </div>
                        <div className="col-md-5 col-xl-5"></div>
                    </div>
                    <br />
                    <br />
                    <div className="row">
                        <div className="d-md-none d-xl-block col-xl-2"></div>
                        <div className="col-md-12 col-xl-8">
                            <div className="restaurantsList">
                                {
                                    this.props.restaurantsList.map((restaurant, i) => <RestaurantsItem key={i} restaurant={restaurant} />)
                                }
                            </div>
                        </div>  
                        <div className="d-md-none d-xl-block col-xl-2"></div>
                    </div>

                </div>
            </div >
        );
    }
}

export function RestaurantsItem(props) {
    return (
        <div className="restaurantItem">
            <div>
                <img className="logoRes" src={props.restaurant.logo} />
            </div>
            <div className="nameRes">{props.restaurant.name}</div>
            <div>
                <Link to={{
                    pathname: "/restaurant",
                    state: 
                    {id: props.restaurant.id}

                }} >
                    <button type="button" className="btn btnRes" >نمایش منو</button>
                </Link>
            </div>

        </div>
    );
}


