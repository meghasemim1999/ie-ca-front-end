import React from 'react';
import AlertError from '../utilities/alert-error';

export default class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchByFood: '',
            searchByRestaurant:'',
        }

        this.handleInput = this.handleInput.bind(this);
    }

    handleInput (event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]: value});
    }
    

    render() {
        return (
            <div className="row fluid">
                <div className="col-md-3 col-xl-3"></div>
                <div className="col-md-6 col-xl-6 ">
                    <div className="row search-box">
                        <SearchField searchBoxTitle="نام غذا" onChange={this.handleInput} name="searchByFood"/>
                        <SearchField searchBoxTitle="نام رستوران" onChange={this.handleInput} name="searchByRestaurant"/>
                        <div className="col-md-4 col-xl-4 search-box-item">
                            <input type="button" className='btn search-botton' value="جست وجو" onClick={(event) => {this.props.onSearch(event, this.state.searchByRestaurant, this.state.searchByFood);}}/>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-xl-3"></div>
            </div>
        );
    }
}

export class SearchField extends React.Component {
    render() {
        return (
            <div className="col-md-4 col-xl-4 search-box-item">
                <div className="form-group">
                    <input type="text" className="form-control input-box" name={this.props.name} id="search" placeholder={this.props.searchBoxTitle} onChange={(event) => this.props.onChange(event)} />
                </div>
            </div>
        );
    }
}